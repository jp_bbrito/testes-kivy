from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label

class Tela(BoxLayout):
	def __init__(self,**kwargs):
		super(Tela, self).__init__(**kwargs)
		self.button = Button(text="Botão 1", font_size=30, on_release = self.incrementar)
		self.label = Label(text='1')
		self.add_widget(self.label)
		self.add_widget(self.button)
	
	def incrementar():
		pass
		#self.button.text = "Soltei"
		#self.label.text = str(int(self.label.text)+1)

class MyApp(App):
	def build(self):
		return Tela()
		
if __name__ == "__main__":
	MyApp().run()
