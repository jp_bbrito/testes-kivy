from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label

class MyApp(App):
	def build(self):
		box = BoxLayout()
		button = Button(text="Botão 1")
		label = Label(text="Layout 1")
		box.add_widget(label)
		box.add_widget(button)
		
		box2 = BoxLayout(orientation="vertical")
		button2 = Button(text="Botão 2")
		label2 = Label(text="Layout 2")
		box2.add_widget(label2)
		box2.add_widget(button2)
		
		box.add_widget(box2)		
		
		return box
		
if __name__ == "__main__":
	MyApp().run()
