from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen

class Gerenciador(ScreenManager):
    pass

class Capa(Screen):
    pass


class SpotfyApp(App):
    def build(self):
        return Gerenciador()

if __name__ == "__main__":
    MySpotfy().run()
